%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-15
Description:    Calculates the Roe flux with the expansion wave restored exactly for real gases
%}

function flux_Roe_StARS = Roe_StARS(W_prim_L, W_prim_R, E_L, E_R, T_L, T_R)
%{
Inputs:         W_prim = 3 x n_cells matrix of primitive variables [kg m^-3; m s^-2; Pa]
                E = vector of volume-specific total energies [J m^-3]
                T = vector of temperatures [K]
Outputs:        flux = vector of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2, J m^-2 s^-2] 
%}
    global M 
    
    % Left region "L" conditions
    rho_L = W_prim_L(1,:);
    v_L = M ./ rho_L;
    u_L = W_prim_L(2,:);
    p_L = W_prim_L(3,:);
    sos_L = getSOS(v_L, T_L);
    H_L = (E_L + p_L)./rho_L;
    flux_L = [rho_L.*u_L; rho_L.*(u_L).^2 + p_L; u_L.*(E_L + p_L)];

    % Right region "R" conditions
    rho_R = W_prim_R(1,:);
    v_R = M ./ rho_R;
    u_R = W_prim_R(2,:);
    p_R = W_prim_R(3,:);
    sos_R = getSOS(v_R, T_R);
    H_R = (E_R + p_R)./rho_R;
    flux_R = [rho_R.*u_R; rho_R.*(u_R).^2 + p_R; u_R.*(E_R + p_R)];

    % Compute Roe averages
    rho_avg = sqrt(rho_L).*sqrt(rho_R);
    p_avg = (sqrt(rho_L).*p_L + sqrt(rho_R).*p_R)./(sqrt(rho_L) + sqrt(rho_R));
    u_avg = (sqrt(rho_L).*u_L + sqrt(rho_R).*u_R)./(sqrt(rho_L) + sqrt(rho_R));
    H_avg = (sqrt(rho_L).*H_L + sqrt(rho_R).*H_R)./(sqrt(rho_L) + sqrt(rho_R));
    v_avg = (sqrt(rho_L).*v_L + sqrt(rho_R).*v_R)./(sqrt(rho_L) + sqrt(rho_R));
    T_avg = (sqrt(rho_L).*T_L + sqrt(rho_R).*T_R)./(sqrt(rho_L) + sqrt(rho_R));
    sos_avg = getSOS(v_avg, T_avg);

    flux_Roe_StARS = ones(3, length(W_prim_L));
    for i = 1:length(flux_Roe_StARS)
        % Compute the wavespeeds / eigenvalues
        lambda = abs([min(u_avg(i) - sos_avg(i), u_L(i) - sos_L(i)); 
            u_avg(i); 
            max(u_avg(i) + sos_avg(i), u_R(i) + sos_R(i))]);

        % Compute the right eigenvectors
        K = [1, 1, 1; 
            u_avg(i) - sos_avg(i), u_avg(i), u_avg(i) + sos_avg(i); 
            H_avg(i) - u_avg(i)*sos_avg(i), u_avg(i)^2/2, H_avg(i) + u_avg(i)*sos_avg(i)];
    
        % Compute the differences in primitive variables
        d_rho = rho_R(i) - rho_L(i);
        d_u = u_R(i) - u_L(i);
        d_p = p_R(i) - p_L(i);
        
        % Compute the wave strengths
        dV = [(d_p - rho_avg(i)*sos_avg(i)*d_u)/(2*sos_avg(i)^2);
            -(d_p/sos_avg(i)^2 - d_rho); 
            (d_p + rho_avg(i)*sos_avg(i)*d_u)/(2*sos_avg(i)^2)];
    
        % Compute the Roe flux
        flux_Roe_StARS(:,i) = (flux_L(:,i) + flux_R(:,i) - K*(lambda.*dV))/2;

        D_lambda = max([0, u_avg(i) - sos_avg(i) - (u_L(i) - sos_L(i))]); % entropy violation bounding term, see Harten & Hyman Eqs. 2.14a and A.10b
        if (lambda(1) < D_lambda) % Left expansion wave - see Wang & Hickey (2020)
%         if u_L(i) < sos_L(i) && u_avg(i) >= sos_avg(i) % Left expansion wave - see Wang & Hickey (2020)
            A_2_LEW = -0.5*log(v_L.*v_avg) + 0.5*log(v_L./v_avg).*(u_L - sos_L + u_avg - sos_avg)./(u_L - sos_L - u_avg + sos_avg);
            v_LEW = exp(-A_2_LEW);        
            rho_LEW = M./v_LEW;
            A_4_LEW = -0.5*log(p_L.*p_avg) + 0.5*log(p_L./p_avg).*(u_L - sos_L + u_avg - sos_avg)./(u_L - sos_L - u_avg + sos_avg);
            p_LEW = exp(-A_4_LEW);
            A_5_LEW = 0.5*(u_L + u_avg) - 0.5*(u_L - sos_L + u_avg - sos_avg).*(u_L - u_avg)./(u_L - sos_L - u_avg + sos_avg);
            u_LEW = A_5_LEW;
            T_LEW = getTfromPV(p_LEW, v_LEW);
            e_LEW = getEfromVT(v_LEW, T_LEW);

            flux_Roe_StARS(:,i) = [rho_LEW(i).*u_LEW(i); 
                rho_LEW(i).*u_LEW(i).^2 + p_LEW(i); 
                u_LEW(i).*(e_LEW(i)./v_LEW(i) + (1/2)*rho_LEW(i).*u_LEW(i).^2 + p_LEW(i))];
            fprintf("LEW at %d...\n", i);
            continue;
        end
        D_lambda = max([0, u_R(i) + sos_R(i) - (u_avg(i) + sos_avg(i))]); % entropy violation bounding term, see Harten & Hyman Eqs. 2.14a and A.10b
        if (lambda(3) < D_lambda) % Right expansion wave 
%         elseif -u_avg(i) >= sos_avg(i) && -u_R(i) < sos_R(i) % Right expansion wave 
            A_2_REW = -0.5*log(v_R.*v_avg) + 0.5*log(v_R./v_avg).*(u_R + sos_R + u_avg + sos_avg)./(u_R + sos_R - u_avg - sos_avg);
            v_REW = exp(-A_2_REW);
            rho_REW = M./v_REW;
            A_4_REW = -0.5*log(p_R.*p_avg) + 0.5*log(p_R./p_avg).*(u_R + sos_R + u_avg + sos_avg)./(u_R + sos_R - u_avg - sos_avg);
            p_REW = exp(-A_4_REW);
            A_5_REW = 0.5*(u_R + u_avg) - 0.5*(u_R + sos_R + u_avg + sos_avg).*(u_R - u_avg)./(u_R + sos_R - u_avg - sos_avg);
            u_REW = A_5_REW;
            T_REW = getTfromPV(p_REW, v_REW);
            e_REW = getEfromVT(v_REW, T_REW);
            
            flux_Roe_StARS(:,i) = [rho_REW(i).*u_REW(i);
                rho_REW(i).*u_REW(i).^2 + p_REW(i);
                u_REW(i).*(e_REW(i)./v_REW(i) + (1/2)*rho_REW(i).*u_REW(i).^2 + p_REW(i))];
            fprintf("REW at %d...\n", i);
            continue;
        end
    end
end