%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-15
Description:    Calculates temperature of a pure gas given e, v, some gas properties, and choice of Equation of State (EOS). Uses iterative solver.
%}

function [T_out] = getTfromEV(e, v, type_solve)
%{
Inputs:         e = vector of internal energies [J kmol^-1]
                v = vector of molar specific volumes [m^3 mol^-1]
                type_solve = solver type {"fsolve", "Newton", "fit"}
Outputs:        T = vector of temperatures [K]
%}
    global A a delta epsilon getTfromERho_fit M omega R T_c type_eos
    crit_conv = 1.0e-6; % convergence criterion

    % Configure EOS
    syms T
    switch type_eos
        case "IG"
            T_out = e .* 0.4 ./ R; 
            return;
            Theta = 0;
            dThetadT = 0;
            d2ThetadT2 = 0;
        case "RK"
            Theta = a*(1./T).^0.5;
            dThetadT = -0.5*a*T.^-1.5;
            d2ThetadT2 = -0.25*a./(T.^2.5) + a./(T.^2.5);
        case "SRK"
            Theta = a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).^2;
            dThetadT = -a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).*(0.48 + ...
                1.574*omega - 0.176*omega^2)./(T.*T_c).^0.5; 
            d2ThetadT2 = 0.5*a*(0.48 + 1.574*omega - 0.176*omega^2)^2./(T.*T_c) + ...
                0.5*a*(1 + (0.48 + 1.574*omega - 0.176*omega^2).*(1 - (T./T_c).^0.5)).*(0.48 + 1.574*omega - 0.176*omega^2)./(T.^1.5.*T_c.^0.5);
        case "PR"
            Theta = a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).^2;
            dThetadT = -a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).*(0.37464 + ...
                1.54226*omega - 0.2699*omega^2)./(T.*T_c).^0.5;
            d2ThetadT2 = 0.5*a*(0.37464 + 1.54226*omega - 0.2699*omega^2)^2./(T.*T_c) + ...
                0.5*a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2).*(1 - (T./T_c).^0.5)).*(0.37464 + 1.54226*omega - 0.2699*omega^2)./(T.^1.5.*T_c.^0.5);
    end

    % Define symbolic expression for c_p_IG_TP
    syms x
    for i = 1:length(A) 
        T_mat(i,1) = x^(i-1); 
    end
    c_p_IG_TP = R*(A*T_mat);
        
    for i=1:length(e)
        if type_solve == "fsolve" % OPTION 1: FSOLVE
            T_cur = 1;
            f_cur = @(x) e(i) - getEfromVT(v(i), x);
            T_out(i) = fsolve(f_cur, T_cur, optimoptions('fsolve', 'Display', 'off'));
        elseif type_solve == "Newton" % OPTION 2: NEWTON-RAPHSON
            T_cur = 1;
            e_cur = getEfromVT(v(i), T_cur);
            diff = e_cur - e(i);
            
            max_iters = 10;
            while (max(abs(diff)) > crit_conv) && max_iters > 1
                if type_eos == "IG"
                    intd2pdT2 = 0;
                else
                    intd2pdT2 = real(2*double(subs(d2ThetadT2, T_cur)).*atanh((2.*v(i) + delta)./(delta^2 - 4*epsilon).^0.5)./(delta^2 - 4*epsilon).^0.5);
                end
                c_v_IG_TP = double(subs(c_p_IG_TP, T_cur)) - R;
                c_v = c_v_IG_TP + T_cur.*intd2pdT2;
    
                T_cur = T_cur - diff / c_v;
                e_cur = getEfromVT(v(i), T_cur);
                diff = e_cur - e(i);
                max_iters = max_iters - 1;
            end
    
            T_out(i) = T_cur;
            fprintf("T_out(%d) Newton = %f\n", i, T_out(i));
        elseif type_solve == "fit"
            T_out(i) = getTfromERho_fit(e(i), M/v(i));
        end

        
    end
end