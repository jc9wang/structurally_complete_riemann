%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-17
Description:    Converts matrix of primitive variables into matrix of conservative variables
%}

function W_cons = primToCons(W_prim)
%{
Inputs:         W_prim = 3 x n_cells matrix of primitive variables [kg m^-3; m s^-2; Pa]
Outputs:        W_cons = 3 x n_cells matrix of conservative variables [kg m^-3; kg m^-2 s^-1, J m^-3] 
%}

global M

    rho = W_prim(1,:);
    v = M ./ rho;
    u = W_prim(2, :);
    p = W_prim(3, :);
    
    T = getTfromPV(p, v);
    E = getEfromVT(v, T);

    W_cons(1, :) = rho;
    W_cons(2, :) = rho .* u;
    W_cons(3, :) = E ./ v + (1/2)*rho.*u.^2;
end