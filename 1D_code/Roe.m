%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-13
Description:    Calculates the Roe flux
%}

function flux_Roe = Roe(W_prim_L, W_prim_R, E_L, E_R, T_L, T_R)
%{
Inputs:         W_prim = 3 x n_cells matrix of primitive variables [kg m^-3; m s^-2; Pa]
                E = vector of volume-specific total energies [J m^-3]
                T = vector of temperatures [K]
Outputs:        flux = vector of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2, J m^-2 s^-2] 
%}
    global M 
    
    % Left region "L" conditions
    rho_L = W_prim_L(1,:);
    v_L = M ./ rho_L;
    u_L = W_prim_L(2,:);
    p_L = W_prim_L(3,:);
    H_L = (E_L + p_L)./rho_L;
    flux_L = [rho_L.*u_L; rho_L.*(u_L).^2 + p_L; u_L.*(E_L + p_L)];

    % Right region "R" conditions
    rho_R = W_prim_R(1,:);
    v_R = M ./ rho_R;
    u_R = W_prim_R(2,:);
    p_R = W_prim_R(3,:);
    H_R = (E_R + p_R)./rho_R;
    flux_R = [rho_R.*u_R; rho_R.*(u_R).^2 + p_R; u_R.*(E_R + p_R)];

    % Compute Roe averages
    rho_avg = sqrt(rho_L).*sqrt(rho_R);
    u_avg = (sqrt(rho_L).*u_L + sqrt(rho_R).*u_R)./(sqrt(rho_L) + sqrt(rho_R));
    H_avg = (sqrt(rho_L).*H_L + sqrt(rho_R).*H_R)./(sqrt(rho_L) + sqrt(rho_R));
    v_avg = (sqrt(rho_L).*v_L + sqrt(rho_R).*v_R)./(sqrt(rho_L) + sqrt(rho_R));
    T_avg = (sqrt(rho_L).*T_L + sqrt(rho_R).*T_R)./(sqrt(rho_L) + sqrt(rho_R));
    sos_avg = getSOS(v_avg, T_avg);
    
    flux_Roe = ones(3, length(W_prim_L));
    for i = 1:length(flux_Roe)
        % Compute the wavespeeds / eigenvalues
        lambda = abs([u_avg(i) - sos_avg(i); 
            u_avg(i); 
            u_avg(i) + sos_avg(i)]);
    
        % Compute the right eigenvectors
        K = [1, 1, 1; 
            u_avg(i) - sos_avg(i), u_avg(i), u_avg(i) + sos_avg(i); 
            H_avg(i) - u_avg(i)*sos_avg(i), u_avg(i)^2/2, H_avg(i) + u_avg(i)*sos_avg(i)];
    
        % Compute the differences in primitive variables
        d_rho = rho_R(i) - rho_L(i);
        d_u = u_R(i) - u_L(i);
        d_p = p_R(i) - p_L(i);
        
        % Compute the wave strengths
        dV = [(d_p - rho_avg(i)*sos_avg(i)*d_u)/(2*sos_avg(i)^2);
            -(d_p/sos_avg(i)^2 - d_rho); 
            (d_p + rho_avg(i)*sos_avg(i)*d_u)/(2*sos_avg(i)^2)];
    
        % Compute the Roe flux
        flux_Roe(:,i) = (flux_L(:,i) + flux_R(:,i) - K*(lambda.*dV))/2;
    end
end