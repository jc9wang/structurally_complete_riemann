%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-22
Description:    Chooses the test case and configs to run
%}

function [p, rho, v, T, u, t_final, CFL, n_cells, dx, x, type_bc, xlim_min, xlim_max] = selectTest(name_test)
%{
Inputs:         name_test = string representing test name
Outputs:        vectors of initial conditions
                grid and other configuration parameters
%}

global M

switch name_test
    case "grad"
        %% Gradient test - for entropy violations in the limit as gradients approach the Riemann problem initial conditions
        CFL = 0.5;
        x_min = -0.5;
        x_max = 0.5;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2;
        type_bc = "transparent"; 
        xlim_min = -0.4;
        xlim_max = 0.4;
        t_final = 0.0005;
        
        dx_mid = 11*dx; % go from 1 dx to 7 dx in increments of 2 dx
        p_max = 1.1e7;
        p_min = 2e5;
        p = max(min([(p_max - p_min)/(dx_mid)*(-x).*ones(1,n_cells) + (p_max + p_min)/2], p_max), p_min);
        rho_max = 180;
        rho_min = 7.4;
        rho = max(min([(rho_max - rho_min)/(dx_mid)*(-x).*ones(1,n_cells) + (rho_max + rho_min)/2], rho_max), rho_min);
        v = M./rho;
        T = [getTfromPV(p, v)];
        u_max = 150;
        u_min = 150;
        u = max(min([(u_max - u_min)/(dx_mid)*(-x).*ones(1,n_cells) + (u_max + u_min)/2], u_max), u_min);

    case "trans_rcs"
        %% Transcritical RCS shock tube test
        CFL = 0.5;
        x_min = -1;
        x_max = 1;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "transparent"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.0009;

        p = [1.1e7*ones(1, n_cells/2), 2e5*ones(1, n_cells/2)]; 
        rho = [180*ones(1, n_cells/2), 7.4*ones(1, n_cells/2)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [150*ones(1, n_cells/2), 50*ones(1, n_cells/2)];

    case "trans_rcs_2"
        %% Transcritical RCS shock tube test w/ periodic bounds
        CFL = 0.5;
        x_min = -1;
        x_max = 1;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "periodic";
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.002;

        p = [1.1e7*ones(1, n_cells/2), 2e5*ones(1, n_cells/2)]; 
        rho = [290*ones(1, n_cells/2), 7.4*ones(1, n_cells/2)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [150*ones(1, n_cells/2), 50*ones(1, n_cells/2)];
end