%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-17
Description:    Calculates intercell fluxes
%}

function flux = getFlux(W_prim, E, T)
%{
Inputs:         W_prim = 3 x n_cells matrix of primitive variables [kg m^-3; m s^-2; Pa]
                E = vector of volume-specific total energies [J m^-3]
                T = vector of temperatures [K]
Outputs:        flux = vector of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2, J m^-2 s^-2] 
%}
    global dx n_cells type_bc type_riemann 

    % First-order reconstruction with periodic boundaries
    W_prim_L = [W_prim(:,n_cells), W_prim(:,1:n_cells)];
    W_prim_R = [W_prim(:,1:n_cells), W_prim(:,1)];
    E_L = [E(:,n_cells), E(:,1:n_cells)];
    E_R = [E(:,1:n_cells), E(:,1)];
    T_L = [T(:,n_cells), T(:,1:n_cells)];
    T_R = [T(:,1:n_cells), T(:,1)];

    switch type_bc % first-order reconstruction
        case "transparent" % boundaries transmit waves, i.e. far-field conditions - see Toro (2009) §6.3.3
            W_prim_L = [W_prim(:,1), W_prim(:,1:n_cells)];
            W_prim_R = [W_prim(:,1:n_cells), W_prim(:,n_cells)];
            E_L = [E(:,1), E(:,1:n_cells)];
            E_R = [E(:,1:n_cells), E(:,n_cells)];
            T_L = [T(:,1), T(:,1:n_cells)];
            T_R = [T(:,1:n_cells), T(:,n_cells)];
        case "reflective" % boundaries reflect, i.e. wall - see Toro (2009) §6.3.3
            W_prim_L = [W_prim(:,1), W_prim(:,1:n_cells)];
            W_prim_L(:,1) = [W_prim_L(1,1); -W_prim_L(2,1); W_prim_L(3,1)];
            W_prim_R = [W_prim(:,1:n_cells), W_prim(:,n_cells)];
            W_prim_R(:,end) = [W_prim_R(1,end); -W_prim_R(2,end); W_prim_R(3,end)];
            E_L = [E(:,1), E(:,1:n_cells)];
            E_R = [E(:,1:n_cells), E(:,n_cells)];
            T_L = [T(:,1), T(:,1:n_cells)];
            T_R = [T(:,1:n_cells), T(:,n_cells)];
        case "periodic" % boundary fluxes pass over from one end to the other
            W_prim_L = [W_prim(:,n_cells), W_prim(:,1:n_cells)];
            W_prim_R = [W_prim(:,1:n_cells), W_prim(:,1)];
            E_L = [E(:,n_cells), E(:,1:n_cells)];
            E_R = [E(:,1:n_cells), E(:,1)];
            T_L = [T(:,n_cells), T(:,1:n_cells)];
            T_R = [T(:,1:n_cells), T(:,1)];
    end

    switch type_riemann
        case "Roe"
            flux_L = Roe(W_prim_L(:,1:n_cells), W_prim_R(:,1:n_cells), E_L(:,1:n_cells), E_R(:,1:n_cells), T_L(:,1:n_cells), T_R(:,1:n_cells));
            flux_R = Roe(W_prim_L(:,2:n_cells+1), W_prim_R(:,2:n_cells+1), E_L(:,2:n_cells+1), E_R(:,2:n_cells+1), T_L(:,2:n_cells+1), T_R(:,2:n_cells+1));
        case "Roe-StARS"
            flux_L = Roe_StARS(W_prim_L(:,1:n_cells), W_prim_R(:,1:n_cells), E_L(:,1:n_cells), E_R(:,1:n_cells), T_L(:,1:n_cells), T_R(:,1:n_cells));
            flux_R = Roe_StARS(W_prim_L(:,2:n_cells+1), W_prim_R(:,2:n_cells+1), E_L(:,2:n_cells+1), E_R(:,2:n_cells+1), T_L(:,2:n_cells+1), T_R(:,2:n_cells+1));
        case "Roe-Harten"
            flux_L = Roe_Harten(W_prim_L(:,1:n_cells), W_prim_R(:,1:n_cells), E_L(:,1:n_cells), E_R(:,1:n_cells), T_L(:,1:n_cells), T_R(:,1:n_cells));
            flux_R = Roe_Harten(W_prim_L(:,2:n_cells+1), W_prim_R(:,2:n_cells+1), E_L(:,2:n_cells+1), E_R(:,2:n_cells+1), T_L(:,2:n_cells+1), T_R(:,2:n_cells+1));
        case "exact"
            flux_L = 0; % TODO
            flux_R = 0; % TODO
    end

    flux = -(flux_R - flux_L)/dx;
end